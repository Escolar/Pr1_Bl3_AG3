#import <stdio.h>

int K();

int M();

int S();

int A();

int D();

int R();

int OG();

int SZ();

int OE();

void OS();

int x;

int main() {
    printf("Bitte geben Sie das Jahr ein: ");
    scanf("%i", &x);

    OS();

    return 0;
}

int K() {
    return x / 100;
}

int M() {
    return 15 + (3 * K(x) + 3) / 4 - (8 * K(x) + 13) / 25;
}

int S() {
    return 2 - (3 * K(x) + 3) / 4;
}

int A() {
    return x % 19;
}

int D() {
    return (19 * A() + M()) % 30;
}

int R() {
    return (D() + A() / 11) / 29;
}

int OG() {
    return 21 + D() - R();
}

int SZ() {
    return 7 - (x + x / 4 + S()) % 7;
}

int OE() {
    return 7 - (OG() - SZ()) % 7;
}

void OS() {

    int date = OG() + OE();

    if(date < 31) {
        printf("\nOstern faellt auf den %d. Maerz %i", OG() + OE(), x);
    } else {
        printf("\nOstern faellt auf den %d. April %i", OG() + OE() - 31, x);
    }


}